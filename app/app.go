package app

import (
	"gitlab.com/pflipp/go-thumbler/engine"
	"gitlab.com/pflipp/go-thumbler/img/encoding"
	"gitlab.com/pflipp/go-thumbler/img/resize"
	"gitlab.com/pflipp/go-thumbler/img/size"
	"gitlab.com/pflipp/go-thumbler/process"
	"gitlab.com/pflipp/go-thumbler/storage"
)

var app *App

func init() {
	app = &App{initializeFunc: func() error {

		return nil
	}}
}

type InitializeFunc func() error

type App struct {
	cacheStorage       storage.ReadWritable
	processingContexts []process.ProcessingContext
	engine             engine.Engine
	resizers           resize.ResizerList
	sizes              size.SizeList
	encoders           encoding.EncodersList
	initializeFunc     InitializeFunc
}

func (a *App) SetEngine(engine engine.Engine) {
	a.engine = engine
}

func (a *App) GetEngine() engine.Engine {

	return a.engine
}

func (a *App) SetCacheStorage(cacheStorage storage.ReadWritable) {

	a.cacheStorage = cacheStorage
}

func (a *App) GetCacheStorage() storage.ReadWritable {

	return a.cacheStorage
}

func (a *App) RegisterProcessingContext(processingContext process.ProcessingContext) {
	a.processingContexts = append(a.processingContexts, processingContext)
}

func (a *App) GetProcessingContexts() []process.ProcessingContext {

	return a.processingContexts
}

func (a *App) RegisterResizers(resizers resize.ResizerList) {
	a.resizers = resizers
}

func (a *App) GetResizers() resize.ResizerList {

	return a.resizers
}

func (a *App) RegisterSizes(sizes size.SizeList) {
	a.sizes = sizes
}

func (a *App) GetSizes() size.SizeList {

	return a.sizes
}

func (a *App) RegisterEncoders(encoders encoding.EncodersList) {
	a.encoders = encoders
}

func (a *App) GetEncoders() encoding.EncodersList {

	return a.encoders
}

func (a *App) OnInitialize(initializeFunc InitializeFunc) {
	a.initializeFunc = initializeFunc
}

func (a *App) Boot() error {
	if err := a.initializeFunc(); err != nil {

		return err
	}

	if err := a.engine.Start(); err != nil {

		return err
	}

	return nil
}

func (a *App) Close() {

	if a.engine != nil {
		a.engine.Stop()
	}

	if s, ok := a.cacheStorage.(storage.Closable); ok {
		s.Close()
	}

	for _, processingContext := range a.processingContexts {
		if s, ok := processingContext.Storage.(storage.Closable); ok {
			s.Close()
		}
	}
}

func GetApp() *App {

	return app
}
