package utils

import (
	"gitlab.com/pflipp/go-thumbler/img"
	"math"
)

func ScaleSize(targetSize, sourceSize *img.Dimension) *img.Dimension {
	if targetSize.Height == 0 && targetSize.Width == 0 {

		return nil
	}

	if targetSize.Height == 0 {

		return ScaleToWidth(targetSize.Width, sourceSize)
	}

	if targetSize.Width == 0 {

		return ScaleToHeight(targetSize.Height, sourceSize)
	}

	return ScaleToBestFittingSize(targetSize, sourceSize)
}

func ScaleToWidth(width uint16, size *img.Dimension) *img.Dimension {
	factor := float64(width) / float64(size.Width)

	newHeight := uint16(factor * float64(size.Height))

	return &img.Dimension{Width: width, Height: newHeight}
}

func ScaleToHeight(height uint16, size *img.Dimension) *img.Dimension {
	factor := float64(height) / float64(size.Height)

	newWidth := uint16(factor * float64(size.Width))

	return &img.Dimension{Width: newWidth, Height: height}
}

func ScaleToBestFittingSize(targetSize, sourceSize *img.Dimension) *img.Dimension {
	factor := math.Min(float64(targetSize.Width)/float64(sourceSize.Width), float64(targetSize.Height)/float64(sourceSize.Height))

	newWidth := uint16(factor * float64(sourceSize.Width))
	newHeight := uint16(factor * float64(sourceSize.Height))

	return &img.Dimension{Width: newWidth, Height: newHeight}
}
