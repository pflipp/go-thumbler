package encoding

import (
	"gitlab.com/pflipp/go-thumbler/img"
	"io"
)

type EncodersList map[string]Encoder

type Encoder interface {
	Encode(writer io.Writer, img img.ImageRef) error
	ContentType() string
	Type() string
}

type PngEncoder struct {
}

func (e *PngEncoder) Encode(writer io.Writer, img img.ImageRef) error {

	return img.WritePng(writer)
}
func (e *PngEncoder) ContentType() string {

	return "image/png"
}
func (e *PngEncoder) Type() string {

	return "png"
}

type JpegEncoder struct {
}

func (e *JpegEncoder) Encode(writer io.Writer, img img.ImageRef) error {

	return img.WriteJpeg(writer)
}
func (e *JpegEncoder) ContentType() string {

	return "image/jpeg"
}
func (e *JpegEncoder) Type() string {

	return "jpg"
}

type WebpEncoder struct {
}

func (e *WebpEncoder) Encode(writer io.Writer, img img.ImageRef) error {

	return img.WriteWebp(writer)
}
func (e *WebpEncoder) ContentType() string {

	return "image/webp"
}
func (e *WebpEncoder) Type() string {

	return "webp"
}

type AvifEncoder struct {
}

func (e *AvifEncoder) Encode(writer io.Writer, img img.ImageRef) error {

	return img.WriteAvif(writer)
}
func (e *AvifEncoder) ContentType() string {

	return "image/avif"
}
func (e *AvifEncoder) Type() string {

	return "avif"
}

func DefaultEncoders() EncodersList {

	return EncodersList{
		"png":  &PngEncoder{},
		"jpg":  &JpegEncoder{},
		"webp": &WebpEncoder{},
		"avif": &AvifEncoder{},
	}
}
