package resize

import (
	"gitlab.com/pflipp/go-thumbler/img"
)

type ResizerList map[string]Resizer

type Resizer interface {
	Name() string
	ResizeImageTo(image img.ImageRef, size *img.Dimension) (img.ImageRef, error)
	ValidateSize(size *img.Dimension) error
}

func DefaultResizers() ResizerList {

	return ResizerList{
		"bestfit": &Bestfit{},
		"centered": &Centered{},
		"crop": &Crop{},
	}
}
