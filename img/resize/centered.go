package resize

import (
	"errors"
	"gitlab.com/pflipp/go-thumbler/img"
	"gitlab.com/pflipp/go-thumbler/img/utils"
)

type Centered struct {
}

func (r *Centered) Name() string {

	return "centered"
}

func (r *Centered) ResizeImageTo(img img.ImageRef, targetSize *img.Dimension) (img.ImageRef, error) {
	srcSize := img.GetDimension()

	size := utils.ScaleSize(targetSize, srcSize)
	blankImage, err := img.NewBlank(targetSize)
	if err != nil {
		return nil, err
	}

	resized, err := img.ResizeTo(size)
	if err != nil {
		return nil, err
	}

	x := (targetSize.Width / 2) - (size.Width / 2)
	y := (targetSize.Height / 2) - (size.Height / 2)

	return blankImage.AppendAt(resized, x, y)
}

func (r *Centered) ValidateSize(size *img.Dimension) error {
	if size.Width == 0 || size.Height == 0 {

		return errors.New("need a size with width and height")
	}

	return nil
}
