package resize

import (
	"errors"
	"gitlab.com/pflipp/go-thumbler/img"
	"gitlab.com/pflipp/go-thumbler/img/utils"
)

type Crop struct {
}

func (r *Crop) Name() string {

	return "crop"
}

func (r *Crop) ResizeImageTo(img img.ImageRef, targetSize *img.Dimension) (img.ImageRef, error) {
	srcSize := img.GetDimension()

	size := r.getResizeImageSize(srcSize, targetSize)

	resized, err := img.ResizeTo(size)
	if err != nil {
		return nil, err
	}

	return resized.CropTo(targetSize.Width, targetSize.Height)
}

func (r *Crop) ValidateSize(size *img.Dimension) error {
	if size.Width == 0 || size.Height == 0 {

		return errors.New("need a size with width and height")
	}

	return nil
}

func (r *Crop) getResizeImageSize(srcSize *img.Dimension, targetSize *img.Dimension) *img.Dimension {
	if srcSize.Width > srcSize.Height {
		return utils.ScaleToHeight(targetSize.Height, srcSize)
	}

	return utils.ScaleToWidth(targetSize.Width, srcSize)
}
