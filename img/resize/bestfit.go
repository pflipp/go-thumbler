package resize

import (
	"gitlab.com/pflipp/go-thumbler/img"
	"gitlab.com/pflipp/go-thumbler/img/utils"
)

type Bestfit struct {
}

func (r *Bestfit) Name() string {

	return "bestfit"
}

func (r *Bestfit) ResizeImageTo(img img.ImageRef, targetSize *img.Dimension) (img.ImageRef, error) {
	srcSize := img.GetDimension()

	size := utils.ScaleSize(targetSize, srcSize)

	return img.ResizeTo(size)
}

func (r *Bestfit) ValidateSize(size *img.Dimension) error {
	//all sizes allowed

	return nil
}
