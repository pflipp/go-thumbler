package img

import (
	"io"
)

type ImageRef interface {
	GetDimension() *Dimension
	ResizeTo(d *Dimension) (ImageRef, error)
	AppendAt(img ImageRef, x, y uint16) (ImageRef, error)
	CropTo(width, height uint16) (ImageRef, error)
	WritePng(w io.Writer) error
	WriteJpeg(w io.Writer) error
	WriteWebp(w io.Writer) error
	WriteAvif(w io.Writer) error
	NewBlank(d *Dimension) (ImageRef, error)
}

type Dimension struct {
	Width  uint16
	Height uint16
}
