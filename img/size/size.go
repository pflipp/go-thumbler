package size

import (
	"fmt"
	"gitlab.com/pflipp/go-thumbler/img"
	"math"
)

type SizeList map[string]*img.Dimension

func DefaultSizes() SizeList {

	commonFactors := []float64{
		0.33333333333,       // 3:1
		0.7999999999999999,  // 3:2.4
		0.6666666666666666,  // 3:2
		0.75,                // 4:3
		0.5625,              // 16:9
		0.42857142857142855, // 21:9
	}

	commonSizes := []float64{
		64, 105, 120, 150, 240, 250, 300, 320, 375, 412, 450, 480, 500, 560, 600, 740, 768, 812, 896, 960,
		1024, 1080, 1280, 1366, 1440, 1600, 1920, 2400, 2560,
		3440, 3840,
	}

	sizes := SizeList{}
	for _, commonSize := range commonSizes {
		sizes[sizeId(commonSize, 0)] = sizeDimension(commonSize, 0)
		sizes[sizeId(0, commonSize)] = sizeDimension(0, commonSize)
		sizes[sizeId(commonSize, commonSize)] = sizeDimension(commonSize, commonSize)
		for _, factor := range commonFactors {

			decreasedSize := commonSize * factor
			sizes[sizeId(commonSize, decreasedSize)] = sizeDimension(commonSize, decreasedSize)
			sizes[sizeId(decreasedSize, commonSize)] = sizeDimension(decreasedSize, commonSize)
		}
	}

	return sizes
}

func sizeId(width, height float64) string {
	w := uint16(math.Round(width))
	h := uint16(math.Round(height))
	if h == 0 {

		return fmt.Sprintf("%dx", w)
	}
	if w == 0 {

		return fmt.Sprintf("x%d", h)
	}

	return fmt.Sprintf("%dx%d", w, h)
}

func sizeDimension(width, height float64) *img.Dimension {
	w := uint16(math.Round(width))
	h := uint16(math.Round(height))

	return &img.Dimension{Width: w, Height: h}
}
