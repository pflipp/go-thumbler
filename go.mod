module gitlab.com/pflipp/go-thumbler

go 1.16

replace github.com/davidbyttow/govips/v2 => github.com/pflipp/govips/v2 v2.7.1-0.20210730133456-914b18539983

require (
	github.com/davidbyttow/govips/v2 v2.7.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.1
	gocloud.dev v0.20.0
)
