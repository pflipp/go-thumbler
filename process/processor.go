package process

import (
	"fmt"
	"gitlab.com/pflipp/go-thumbler/engine"
	"gitlab.com/pflipp/go-thumbler/img"
	"gitlab.com/pflipp/go-thumbler/img/encoding"
	"gitlab.com/pflipp/go-thumbler/img/resize"
	"gitlab.com/pflipp/go-thumbler/storage"
	"io"
	"strings"
)

type Processor struct {
	cachePrefix   string
	cacheStorage  storage.ReadWritable
	sourceStorage storage.Readable
	encoder       encoding.Encoder
	resizer       resize.Resizer
	size          *img.Dimension
	path          string
	output        io.Writer
	engine        engine.Engine
}

func (p *Processor) Process() error {
	if p.imgIsCached() {

		return p.writeFromCache()
	}

	reader, err := p.sourceStorage.GetReader(p.path)

	if err != nil {

		return err
	}

	defer reader.Close()

	srcImage, err := p.engine.ReadImage(reader.GetReader())

	if err != nil {

		return err
	}

	resizedImage, err := p.resizer.ResizeImageTo(srcImage, p.size)

	if err != nil {

		return err
	}

	if p.useCache() {

		return p.writeToCache(resizedImage)
	}

	return p.encoder.Encode(p.output, resizedImage)
}

func (p *Processor) writeToCache(ref img.ImageRef) error {

	writer, err := p.cacheStorage.GetWriter(p.getCachePath(), storage.Attributes{ContentType: p.encoder.ContentType()})
	if err != nil {

		return err
	}

	if err := p.encoder.Encode(writer, ref); err != nil {
		writer.Close()

		return err
	}

	writer.Close()

	return p.writeFromCache()
}

func (p *Processor) writeFromCache() error {

	reader, err := p.cacheStorage.GetReader(p.getCachePath())

	if err != nil {

		return nil
	}
	defer reader.Close()

	if _, err := reader.WriteTo(p.output); err != nil {

		return err
	}

	return nil
}

func (p *Processor) imgIsCached() bool {
	if !p.useCache() {

		return false
	}

	cachePath := p.getCachePath()

	exists, err := p.cacheStorage.Exists(cachePath)

	return exists && err == nil
}

func (p *Processor) useCache() bool {
	if p.cacheStorage == nil || p.cachePrefix == "" {

		return false
	}

	return true
}

func (p *Processor) getCachePath() string {

	return fmt.Sprintf("%s~%s~%s~%dx%d.%s", p.cachePrefix, strings.ReplaceAll(p.path, "/", "~"), p.resizer.Name(), p.size.Width, p.size.Height, p.encoder.Type())
}

func (p *Processor) UseEngine(engine engine.Engine) *Processor {
	p.engine = engine

	return p
}

func (p *Processor) WithCache(storage storage.ReadWritable, prefix string) *Processor {
	p.cacheStorage = storage
	p.cachePrefix = prefix

	return p
}

func (p *Processor) FromSource(storage storage.Readable, path string) *Processor {
	p.sourceStorage = storage
	p.path = path

	return p
}

func (p *Processor) WithResizer(resizer resize.Resizer) *Processor {
	p.resizer = resizer

	return p
}

func (p *Processor) UseEncoder(encoder encoding.Encoder) *Processor {
	p.encoder = encoder

	return p
}

func (p *Processor) ToSize(size *img.Dimension) *Processor {
	p.size = size

	return p
}

func (p *Processor) ToOutput(output io.Writer) *Processor {
	p.output = output

	return p
}
