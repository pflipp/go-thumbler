package process

import "gitlab.com/pflipp/go-thumbler/storage"

type ProcessingPatternMatches map[string]string
type ProcessingPatternResolver func(matches ProcessingPatternMatches) (string, error)
type ProcessingContext struct {
	Id              string
	Pattern         string
	Storage         storage.Readable
	PatternResolver ProcessingPatternResolver
}
