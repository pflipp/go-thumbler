package vips

import (
	"errors"
	"fmt"
	govips "github.com/davidbyttow/govips/v2/vips"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pflipp/go-thumbler/img"
	"io"
)

type Vips struct {
}

func (v *Vips) Start() error {
	govips.LoggingSettings(func(messageDomain string, messageLevel govips.LogLevel, message string) {
		switch messageLevel {
		case govips.LogLevelInfo:
		case govips.LogLevelMessage:
			log.Infof("%s: %s", messageDomain, message)
		case govips.LogLevelWarning:
			log.Warnf("%s: %s", messageDomain, message)
		case govips.LogLevelCritical:
			log.Errorf("%s: %s", messageDomain, message)
		case govips.LogLevelError:
			log.Fatalf("%s: %s", messageDomain, message)
		}

		log.Debugf("%s: %s", messageDomain, message)
	}, govips.LogLevelDebug)

	govips.Startup(nil)

	return nil
}

func (v *Vips) Stop() {
	govips.Shutdown()
}

func (v *Vips) ReadImage(r io.Reader) (img.ImageRef, error) {
	govipsImage, err := govips.NewImageFromReader(r)
	if err != nil {

		return nil, err
	}
	log.Debugf("image meta: %v+", govipsImage.Metadata())

	if govipsImage.Format() != govips.ImageTypePDF {

		return &ImageRef{govipsImage: govipsImage}, nil
	}

	left, top, width, height, err := govipsImage.FindTrim(0, &govips.Color{255,255,255})

	if err != nil {

		return nil, err
	}

	if (left == 0 && top == 0 && width == govipsImage.Width() && height == govipsImage.Height()) || (left == govipsImage.Width()&& top == govipsImage.Height() && width == 0 && height == 0) {

		return &ImageRef{govipsImage: govipsImage}, nil
	}

	log.Debugf("found trim: %d, %d, %d, %d", left, top, width, height)

	if err := govipsImage.ExtractArea(left, top, width, height); err != nil {

		return nil, err
	}

	return &ImageRef{govipsImage: govipsImage}, nil
}

type ImageRef struct {
	govipsImage *govips.ImageRef
}

func (vi *ImageRef) GetDimension() *img.Dimension {

	return &img.Dimension{Width: uint16(vi.govipsImage.Width()), Height: uint16(vi.govipsImage.Height())}
}

func (vi *ImageRef) ResizeTo(d *img.Dimension) (img.ImageRef, error) {
	if err := vi.govipsImage.Thumbnail(int(d.Width), int(d.Height), govips.InterestingAll); err != nil {

		return nil, err
	}
	return &ImageRef{govipsImage: vi.govipsImage}, nil
}

func (vi *ImageRef) AppendAt(i img.ImageRef, x, y uint16) (img.ImageRef, error) {
	viImage, ok := i.(*ImageRef)
	if !ok {
		return nil, errors.New("wrong image ref")
	}

	if err := vi.govipsImage.Insert(viImage.govipsImage, int(x), int(y), false, nil); err != nil {

		return nil, err
	}

	return &ImageRef{govipsImage: vi.govipsImage}, nil
}

func (vi *ImageRef) CropTo(width, height uint16) (img.ImageRef, error) {

	if err := vi.govipsImage.SmartCrop(int(width), int(height), govips.InterestingCentre); err != nil {

		return nil, err
	}

	return &ImageRef{govipsImage: vi.govipsImage}, nil
}

func (vi *ImageRef) NewBlank(d *img.Dimension) (img.ImageRef, error) {
	// the easiest way i found to create a blank transparent image for libvips
	blankImage, err := govips.NewImageFromBuffer([]byte(fmt.Sprintf(`<svg viewBox="0 0 %d %d"></svg>`, d.Width, d.Height)))
	if err != nil {
		return nil, err
	}

	return &ImageRef{govipsImage: blankImage}, nil
}

func (vi *ImageRef) WritePng(w io.Writer) error {

	buffer, _, err := vi.govipsImage.ExportPng(&govips.PngExportParams{
		Compression: 6,
		Interlace:   true,
	})

	if err != nil {

		return err
	}

	return writeImageBuffer(buffer, w)
}

func (vi *ImageRef) WriteJpeg(w io.Writer) error {

	if vi.govipsImage.HasAlpha() {

		if err := vi.govipsImage.Flatten(&govips.Color{255, 255, 255}); err != nil {

			return err
		}

	}

	buffer, _, err := vi.govipsImage.ExportJpeg(govips.NewJpegExportParams())

	if err != nil {

		return err
	}

	return writeImageBuffer(buffer, w)
}

func (vi *ImageRef) WriteWebp(w io.Writer) error {

	buffer, _, err := vi.govipsImage.ExportWebp(govips.NewWebpExportParams())

	if err != nil {

		return err
	}

	return writeImageBuffer(buffer, w)
}

func (vi *ImageRef) WriteAvif(w io.Writer) error {

	buffer, _, err := vi.govipsImage.ExportAvif(govips.NewAvifExportParams())

	if err != nil {

		return err
	}

	return writeImageBuffer(buffer, w)
}

func writeImageBuffer(buffer []byte, w io.Writer) error {

	_, err := w.Write(buffer)

	return err
}
