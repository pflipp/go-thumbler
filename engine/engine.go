package engine

import (
	"gitlab.com/pflipp/go-thumbler/img"
	"io"
)

type Engine interface {
	Start() error
	Stop()
	ReadImage(r io.Reader) (img.ImageRef, error)
}
