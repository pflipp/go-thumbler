package main

import (
	"gitlab.com/pflipp/go-thumbler/thumbler/cmd"
)

func main() {

	cmd.Execute()
}
