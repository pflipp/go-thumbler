package cmd

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/pflipp/go-thumbler/app"
	vips2 "gitlab.com/pflipp/go-thumbler/engine/vips"
	"gitlab.com/pflipp/go-thumbler/img/encoding"
	"gitlab.com/pflipp/go-thumbler/img/resize"
	"gitlab.com/pflipp/go-thumbler/img/size"
	"gitlab.com/pflipp/go-thumbler/process"
	"gitlab.com/pflipp/go-thumbler/storage"
)

var rootCmd = &cobra.Command{
	Use:   "thumbler",
	Short: "make thumbs",
}

type rootConfig struct {
	cachePath   string
	storagePath string
	staticPath  string
}

func init() {
	var logLevel, cachePath, storagePath, staticPath string
	rootCmd.PersistentFlags().StringVar(&logLevel, "logLevel", "error", "log level")
	rootCmd.PersistentFlags().StringVar(&cachePath, "cachePath", "", "path to cache thumbs")
	rootCmd.PersistentFlags().StringVar(&storagePath, "storagePath", "", "path to store source images")
	rootCmd.PersistentFlags().StringVar(&staticPath, "staticPath", "", "path where predefined source images are stored")

	cobra.OnInitialize(func() {
		log.SetLevel(log.DebugLevel)

		if lvl, err := log.ParseLevel(logLevel); err == nil {

			log.SetLevel(lvl)
		}

		if err := initRootConfig(&rootConfig{
			cachePath:   cachePath,
			staticPath:  staticPath,
			storagePath: storagePath,
		}); err != nil {
			log.Panic(err)
		}
	})
}

func initRootConfig(config *rootConfig) error {
	app.GetApp().SetEngine(&vips2.Vips{})

	if config.cachePath != "" {
		cacheStorage, err := storage.NewCloud(config.cachePath)
		if err != nil {
			return err
		}

		app.GetApp().SetCacheStorage(cacheStorage)
	}

	if config.staticPath != "" {
		staticStorage, err := storage.NewLocal(config.staticPath)
		if err != nil {
			return err
		}

		app.GetApp().RegisterProcessingContext(process.ProcessingContext{
			Id:      "static",
			Pattern: "static/{context}/{name:[^\\.]+}.{extension:[^\\.]+}",
			Storage: staticStorage,
			PatternResolver: func(vars process.ProcessingPatternMatches) (string, error) {

				return fmt.Sprintf("%s/%s.%s", vars["context"], vars["name"], vars["extension"]), nil
			},
		})
	}

	if config.storagePath != "" {
		mainStorage, err := storage.NewCloud(config.storagePath)
		if err != nil {
			return err
		}

		app.GetApp().RegisterProcessingContext(process.ProcessingContext{
			Id:      "main",
			Pattern: "{id:[a-f0-9]+-[a-f0-9]+-[a-f0-9]+-[a-f0-9]+-[a-f0-9]+}",
			Storage: mainStorage,
			PatternResolver: func(vars process.ProcessingPatternMatches) (string, error) {

				return vars["id"], nil
			},
		})
	}

	app.GetApp().RegisterResizers(resize.DefaultResizers())

	app.GetApp().RegisterSizes(size.DefaultSizes())

	app.GetApp().RegisterEncoders(encoding.DefaultEncoders())

	return app.GetApp().Boot()
}

func Execute() {
	defer app.GetApp().Close()

	if err := rootCmd.Execute(); err != nil {

		log.Panic(err)
	}
}
