package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/pflipp/go-thumbler/app"
	"gitlab.com/pflipp/go-thumbler/server"
	"os"
)

func init() {
	serve := &cobra.Command{
		Use:   "serve",
		Short: "start server",
	}

	var address string

	serve.PersistentFlags().StringVar(&address, "address", ":8000", "listen to address")

	serve.RunE = func(cmd *cobra.Command, args []string) error {
		s := server.New(server.Config{
			Address:              address,
			ApiStaticBearerToken: os.Getenv("THUMBLER_API_INSECURE_STATIC_BEARER_TOKEN"),
		})

		if err := s.MountHandlers(
			app.GetApp().GetSizes(),
			app.GetApp().GetEncoders(),
			app.GetApp().GetResizers(),
			app.GetApp().GetProcessingContexts(),
			app.GetApp().GetEngine(),
			app.GetApp().GetCacheStorage(),
		); err != nil {

			return err
		}

		return s.Serve()
	}

	rootCmd.AddCommand(serve)
}
