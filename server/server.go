package server

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pflipp/go-thumbler/engine"
	"gitlab.com/pflipp/go-thumbler/img/encoding"
	"gitlab.com/pflipp/go-thumbler/img/resize"
	"gitlab.com/pflipp/go-thumbler/img/size"
	"gitlab.com/pflipp/go-thumbler/process"
	"gitlab.com/pflipp/go-thumbler/storage"
	"net/http"
	"time"
)

type Config struct {
	Address              string
	ApiStaticBearerToken string
}

type Server struct {
	config Config
	srv    *http.Server
}

func (s *Server) Serve() error {
	log.Infof("Listening on %s", s.config.Address)

	s.srv.Addr = s.config.Address
	s.srv.WriteTimeout = 15 * time.Second
	s.srv.ReadTimeout = 15 * time.Second

	return s.srv.ListenAndServe()
}

func (s *Server) MountHandlers(sizes size.SizeList, encoders encoding.EncodersList, resizers resize.ResizerList, processingContexts []process.ProcessingContext, engine engine.Engine, cacheStorage storage.ReadWritable) error {
	log.Info("loading routes..")
	r := mux.NewRouter()

	r.Use(loggingMiddleware)

	r.Path("/_status").Methods(http.MethodGet).HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		s.serveJson(writer, struct {
			Status string `json:"status"`
		}{Status: "OK"})
	})
	s.mountWellknownHandlers(r, sizes, encoders, resizers)
	s.mountResizeHandlers(r, processingContexts, engine, sizes, encoders, resizers, cacheStorage)
	if mainStorage := s.getWritableMainStorage(processingContexts); mainStorage != nil && s.config.ApiStaticBearerToken != "" {
		s.mountApiHandlers(r, mainStorage, s.config.ApiStaticBearerToken)
	}

	s.srv.Handler = r

	return nil
}

func (s *Server) serveNotFound(writer http.ResponseWriter, message string) {
	writer.Header().Set("Content-Type", "text/html; charset=utf-8")
	writer.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	writer.WriteHeader(http.StatusNotFound)

	fmt.Fprintf(writer, "<html><head><title>404 Not Found</title></head><body><center><h1>404 Not Found</h1><p>%s</p></center></body></html>", message)
}

func (s *Server) serveInternalServerError(writer http.ResponseWriter, err error) {
	log.Error(err)

	writer.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	http.Error(writer, "ups, there was an error", http.StatusInternalServerError)
}

func (s *Server) serveJson(writer http.ResponseWriter, data interface{}) {

	b, err := json.Marshal(data)
	if err != nil {
		s.serveInternalServerError(writer, err)

		return
	}

	writer.Header().Set("Content-Type", "application/json; charset=utf-8")
	writer.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	writer.WriteHeader(http.StatusOK)

	writer.Write(b)
}

func (s *Server) serveBadRequest(writer http.ResponseWriter, msg string) {

	b, err := json.Marshal(&struct {
		Message string `json:"message"`
	}{msg})

	if err != nil {
		s.serveInternalServerError(writer, err)

		return
	}

	writer.Header().Set("Content-Type", "application/json; charset=utf-8")
	writer.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	writer.WriteHeader(http.StatusBadRequest)

	writer.Write(b)
}

func (s *Server) getWritableMainStorage(processingContexts []process.ProcessingContext) storage.Writable {
	for _, processingContext := range processingContexts {
		if contextStorage, ok := processingContext.Storage.(storage.Writable); ok {

			return contextStorage
		}
	}

	return nil
}

func New(config Config) *Server {

	return &Server{config: config, srv: &http.Server{}}
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Do stuff here
		log.Println(r.RequestURI)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}
