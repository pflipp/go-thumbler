package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/pflipp/go-thumbler/img/encoding"
	"gitlab.com/pflipp/go-thumbler/img/resize"
	"gitlab.com/pflipp/go-thumbler/img/size"
	"net/http"
)

func (s *Server) mountWellknownHandlers(r *mux.Router, sizes size.SizeList, encoders encoding.EncodersList, resizers resize.ResizerList) {
	wellknownRoutes := r.PathPrefix("/.well-known").Methods(http.MethodGet).Subrouter()
	wellknownRoutes.Path("/thumbler").HandlerFunc(s.WellknownThumblerHandler)
	wellknownRoutes.Path("/sizes.json").HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		s.WellknownSizesHandler(writer, sizes)
	})
	wellknownRoutes.Path("/encoders.json").HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		s.WellknownEncodersHandler(writer, encoders)
	})
	wellknownRoutes.Path("/resizers.json").HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		s.WellknownResizersHandler(writer, resizers)
	})
}

func (s *Server) WellknownSizesHandler(writer http.ResponseWriter, sizes size.SizeList) {

	sizesOutput := struct {
		Sizes size.SizeList `json:"sizes"`
	}{Sizes: sizes}

	s.serveJson(writer, sizesOutput)
}

func (s *Server) WellknownEncodersHandler(writer http.ResponseWriter, encoders encoding.EncodersList) {

	encodersMap := map[string]string{}

	for ext, enc := range encoders {
		encodersMap[ext] = enc.ContentType()
	}

	typesOutput := struct {
		Encoders map[string]string `json:"encoders"`
	}{Encoders: encodersMap}

	s.serveJson(writer, typesOutput)
}

func (s *Server) WellknownResizersHandler(writer http.ResponseWriter, resizers resize.ResizerList) {

	resizerIds := make([]string, 0, len(resizers))
	for k := range resizers {
		resizerIds = append(resizerIds, k)
	}

	resizersOutput := struct {
		Resizers []string `json:"resizers"`
	}{Resizers: resizerIds}

	s.serveJson(writer, resizersOutput)
}

func (s *Server) WellknownThumblerHandler(writer http.ResponseWriter, request *http.Request) {

	thumblerOutput := struct {
		Encoders string `json:"encoders"`
		Sizes    string `json:"sizes"`
		Resizers string `json:"resizers"`
		PathSpec string `json:"pathSpec"`
	}{
		Encoders: fmt.Sprintf("%s/.well-known/encoders.json", getRequestedHost(request)),
		Sizes:    fmt.Sprintf("%s/.well-known/sizes.json", getRequestedHost(request)),
		Resizers: fmt.Sprintf("%s/.well-known/resizers.json", getRequestedHost(request)),
		PathSpec: fmt.Sprintf("%s/<resizer>/<size>/<sourcePath>.<encoder>", getRequestedHost(request)),
	}

	s.serveJson(writer, thumblerOutput)
}
