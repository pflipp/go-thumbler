package server

import (
	"bytes"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/pflipp/go-thumbler/engine"
	"gitlab.com/pflipp/go-thumbler/img"
	"gitlab.com/pflipp/go-thumbler/img/encoding"
	"gitlab.com/pflipp/go-thumbler/img/resize"
	"gitlab.com/pflipp/go-thumbler/img/size"
	"gitlab.com/pflipp/go-thumbler/process"
	"gitlab.com/pflipp/go-thumbler/storage"
	"net/http"
	"strconv"
)

const (
	requestContextKeySize    string = "size"
	requestContextKeyEncoder string = "encoder"
	requestContextKeyResizer string = "resizer"
)

func (s *Server) mountResizeHandlers(r *mux.Router, processingContexts []process.ProcessingContext, engine engine.Engine, sizes size.SizeList, encoders encoding.EncodersList, resizers resize.ResizerList, cacheStorage storage.ReadWritable) {
	resizerRoutes := r.PathPrefix(fmt.Sprintf("/{%s}/{%s:[0-9]*x[0-9]*}", requestContextKeyResizer, requestContextKeySize)).Methods(http.MethodGet).Subrouter()

	for _, processingContext := range processingContexts {

		resizerRoutes.Path(fmt.Sprintf("/%s.{%s}", processingContext.Pattern, requestContextKeyEncoder)).HandlerFunc(s.resizeHandleFunc(processingContext, engine, sizes, encoders, resizers, cacheStorage))
	}
}

func (s *Server) getEncoderFromRequestVars(vars map[string]string, encoders encoding.EncodersList) (encoding.Encoder, error) {
	encoderId := vars[requestContextKeyEncoder]

	encoder := encoders[encoderId]
	if encoder == nil {

		return nil, fmt.Errorf("encoder %s not found", encoderId)
	}

	return encoder, nil
}

func (s *Server) getResizerFromRequestVars(vars map[string]string, resizers resize.ResizerList) (resize.Resizer, error) {
	resizerId := vars[requestContextKeyResizer]

	resizer := resizers[resizerId]
	if resizer == nil {

		return nil, fmt.Errorf("resizer %s not found", resizerId)
	}

	return resizer, nil
}

func (s *Server) getSizeFromRequestVars(vars map[string]string, sizes size.SizeList) (*img.Dimension, error) {
	sizeId := vars[requestContextKeySize]

	dimension := sizes[sizeId]
	if dimension == nil {

		return nil, fmt.Errorf("size %s not found", sizeId)
	}

	return dimension, nil
}

func (s *Server) resizeHandleFunc(processingContext process.ProcessingContext, engine engine.Engine, sizes size.SizeList, encoders encoding.EncodersList, resizers resize.ResizerList, cacheStorage storage.ReadWritable) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		path, err := processingContext.PatternResolver(vars)
		if err != nil {
			s.serveNotFound(writer, err.Error())

			return
		}

		exits, err := processingContext.Storage.Exists(path)
		if err != nil {
			s.serveInternalServerError(writer, err)

			return
		}
		if !exits {
			s.serveNotFound(writer, fmt.Sprintf("source %s not exists", path))

			return
		}

		encoder, err := s.getEncoderFromRequestVars(vars, encoders)
		if err != nil {
			s.serveNotFound(writer, err.Error())

			return
		}

		resizer, err := s.getResizerFromRequestVars(vars, resizers)
		if err != nil {
			s.serveNotFound(writer, err.Error())

			return
		}

		dimension, err := s.getSizeFromRequestVars(vars, sizes)
		if err != nil {
			s.serveNotFound(writer, err.Error())

			return
		}

		if err := resizer.ValidateSize(dimension); err != nil {
			s.serveNotFound(writer, err.Error())

			return
		}

		p := &process.Processor{}
		p.WithCache(cacheStorage, processingContext.Id)
		p.UseEngine(engine)
		p.UseEncoder(encoder)
		p.FromSource(processingContext.Storage, path)
		p.WithResizer(resizer)
		p.ToSize(dimension)

		var buf bytes.Buffer

		p.ToOutput(&buf)

		if err := p.Process(); err != nil {
			s.serveInternalServerError(writer, err)

			return
		}

		writer.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d", 86400))
		writer.Header().Set("Content-Type", encoder.ContentType())
		writer.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
		writer.WriteHeader(http.StatusOK)

		buf.WriteTo(writer)
	}
}

func getRequestedHost(request *http.Request) string {

	return fmt.Sprintf("http://%s", request.Host)
}
