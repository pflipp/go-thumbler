package server

import (
	"fmt"
	guuid "github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/pflipp/go-thumbler/storage"
	"io/ioutil"
	"net/http"
)

func (s *Server) mountApiHandlers(r *mux.Router, contextStorage storage.Writable, token string) {
	apiRoutes := r.PathPrefix("/api/v1").Subrouter()
	apiRoutes.Use(authMiddlewareFunc(token))

	apiRoutes.Path("/image").Methods(http.MethodPost).HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		buf, err := ioutil.ReadAll(request.Body)

		if err != nil {
			s.serveInternalServerError(writer, err)

			return
		}

		if len(buf) == 0 {
			s.serveBadRequest(writer, "empty body")

			return
		}

		contentType := http.DetectContentType(buf[:512])

		if !s.contentTypeAllowed(contentType) {
			s.serveBadRequest(writer, fmt.Sprintf("content type %s not allowed", contentType))

			return
		}

		id := guuid.New().String()

		storageWriter, err := contextStorage.GetWriter(id, storage.Attributes{ContentType: contentType})

		if err != nil {
			s.serveInternalServerError(writer, err)

			return
		}

		defer storageWriter.Close()

		if _, err := storageWriter.Write(buf); err != nil {
			s.serveInternalServerError(writer, err)

			return
		}

		okResonse := struct {
			Id string `json:"id"`
		}{id}

		s.serveJson(writer, okResonse)
	})
}

func (s *Server) contentTypeAllowed(contentType string) bool {
	switch contentType {
	case "application/pdf", "image/png", "image/jpg", "image/jpeg", "image/svg+xml":
		return true
	}

	return false
}

func authMiddlewareFunc(token string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			if request.Header.Get("authorization") == "" || request.Header.Get("authorization") != fmt.Sprintf("Bearer %s", token) {
				writer.WriteHeader(http.StatusForbidden)

				return
			}

			next.ServeHTTP(writer, request)
		})
	}
}
