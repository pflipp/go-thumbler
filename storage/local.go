package storage

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"os"
)

type LocalStorage struct {
	path string
}

func (s *LocalStorage) Exists(name string) (bool, error) {
	_, err := os.Stat(fmt.Sprintf("%s/%s", s.path, name))

	if err != nil && os.IsNotExist(err) {

		return false, nil
	}
	if err != nil {

		return false, err
	}

	return true, nil
}

func (s *LocalStorage) GetReader(name string) (ReaderWrapper, error) {

	fi, err := os.Stat(fmt.Sprintf("%s/%s", s.path, name))

	if err != nil {

		return nil, err
	}

	f, err := os.Open(fmt.Sprintf("%s/%s", s.path, name))

	if err != nil {

		return nil, err
	}

	buffer := make([]byte, 512)

	if _, err := f.Read(buffer); err != nil {
		return nil, err
	}

	contentType := http.DetectContentType(buffer)

	if _, err := f.Seek(0, io.SeekStart); err != nil {

		return nil, err
	}

	return &LocalReaderWrapper{
		file:        f,
		contentType: contentType,
		size:        fi.Size(),
	}, nil
}

func NewLocal(path string) (*LocalStorage, error) {

	return &LocalStorage{path: path}, nil
}

type LocalReaderWrapper struct {
	file        *os.File
	contentType string
	size        int64
}

func (lw *LocalReaderWrapper) ContentType() string {

	return lw.contentType
}

func (lw *LocalReaderWrapper) GetReader() io.Reader {

	return lw.file
}

func (lw *LocalReaderWrapper) Close() error {

	return lw.file.Close()
}

func (lw *LocalReaderWrapper) Size() int64 {

	return lw.size
}
func (lw *LocalReaderWrapper) WriteTo(writer io.Writer) (int64, error) {

	return bufio.NewReader(lw.file).WriteTo(writer)
}
