package storage

import (
	"io"
)

type Readable interface {
	Exists(name string) (bool, error)
	GetReader(name string) (ReaderWrapper, error)
}

type Writable interface {
	GetWriter(name string, attrs Attributes) (io.WriteCloser, error)
}

type ReadWritable interface {
	Readable
	Writable
}

type Closable interface {
	Close() error
}

type ReaderWrapper interface {
	ContentType() string
	Close() error
	GetReader() io.Reader
	Size() int64
	WriteTo(writer io.Writer) (int64, error)
}

type Attributes struct {
	ContentType string
}
