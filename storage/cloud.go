package storage

import (
	"context"
	"gocloud.dev/blob"
	_ "gocloud.dev/blob/azureblob"
	_ "gocloud.dev/blob/fileblob"
	_ "gocloud.dev/blob/gcsblob"
	_ "gocloud.dev/blob/s3blob"
	"io"
)

type CloudStorage struct {
	path   string
	bucket *blob.Bucket
}

func (s *CloudStorage) initBucket() error {
	if s.bucket != nil {

		return nil
	}

	bucket, err := blob.OpenBucket(context.Background(), s.path)
	if err != nil {
		return err
	}

	s.bucket = bucket

	return nil
}

func (s *CloudStorage) Exists(name string) (bool, error) {
	if err := s.initBucket(); err != nil {

		return false, err
	}

	return s.bucket.Exists(context.Background(), name)
}

func (s *CloudStorage) Close() error {
	if s.bucket == nil {
		return nil
	}

	return s.bucket.Close()
}

func (s *CloudStorage) GetReader(name string) (ReaderWrapper, error) {
	if err := s.initBucket(); err != nil {

		return nil, err
	}

	r, err := s.bucket.NewReader(context.Background(), name, &blob.ReaderOptions{})

	if err != nil {

		return nil, err
	}

	return &CloudReaderWrapper{r}, nil
}

func (s *CloudStorage) GetWriter(name string, attrs Attributes) (io.WriteCloser, error) {
	if err := s.initBucket(); err != nil {

		return nil, err
	}

	return s.bucket.NewWriter(context.Background(), name, &blob.WriterOptions{ContentType: attrs.ContentType})
}

func NewCloud(path string) (*CloudStorage, error) {

	return &CloudStorage{path: path}, nil
}

func NewCloudWithBucket(bucket *blob.Bucket) *CloudStorage {

	return &CloudStorage{bucket: bucket}
}

type CloudReaderWrapper struct {
	blobReader *blob.Reader
}

func (rw *CloudReaderWrapper) ContentType() string {

	return rw.blobReader.ContentType()
}

func (rw *CloudReaderWrapper) GetReader() io.Reader {

	return rw.blobReader
}

func (rw *CloudReaderWrapper) Close() error {

	return rw.blobReader.Close()
}
func (rw *CloudReaderWrapper) Size() int64 {

	return rw.blobReader.Size()
}

func (rw *CloudReaderWrapper) WriteTo(writer io.Writer) (int64, error) {

	return rw.blobReader.WriteTo(writer)
}
