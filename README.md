# Go Thumbler

just another image resize app.

it uses [libvips](https://github.com/libvips/libvips) with [govips](https://github.com/davidbyttow/govips) under the hood. the base image also includes [PDFium](https://github.com/PDFium/PDFium) for PDF support.

## usage

```bash
 THUMBLER_API_INSECURE_STATIC_BEARER_TOKEN=1234 thumbler --cachePath=./my/cache --staticPath=./my/static/ --storagePath=file://./my/storage/ serve
```

Open http://localhost:8000/.well-known/thumbler

## use as a library

```go
package main

import (
 "gitlab.com/pflipp/go-thumbler/app"
 "gitlab.com/pflipp/go-thumbler/thumbler/cmd"
)

func main() {
 app.GetApp().OnInitialize(func() error {
  //do stuff here, register storages/encoders/resizers/sizes..

  return nil
 })

 cmd.Execute()
}
```
