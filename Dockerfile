FROM pflipp/go-vips as build

WORKDIR /build

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY ./ ./

RUN go mod verify

RUN GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o /thumbler ./thumbler

FROM debian:buster
RUN apt-get update && apt-get -y install --no-install-recommends \
        liborc-0.4-0 \
    	libpng16-16 \
    	librsvg2-2 \
    	libjpeg62 \
    	libexif12 \
    	liblcms2-2 \
    	libheif1 \
    	libwebp6 \
    	libwebpdemux2 \
    	libwebpmux3 \
    && rm -rf /var/lib/apt/lists/*


ENV LD_LIBRARY_PATH=/usr/local/vips/lib

COPY --from=pflipp/go-vips /etc/ssl/certs /etc/ssl/certs
COPY --from=pflipp/go-vips /usr/local/bin /usr/local/bin

RUN true

COPY --from=pflipp/go-vips /usr/local/lib /usr/local/lib
COPY --from=pflipp/go-vips /usr/local/etc /usr/local/etc

RUN true

COPY --from=pflipp/go-vips /usr/local/vips /usr/local/vips

RUN true

COPY --from=build /thumbler /usr/bin/thumbler

ENTRYPOINT ["thumbler"]
